import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  private chosenWord: string;
  private displayWord: string;
  private chosenCategory: string[];
  private category: string;

  private randomWord: string;
  private randomWord2: string;

  private cars: string[] = ["Car Brands", "Ford", "Honda", "Toyota", "Chevrolet", "Buick"];
  private programming: string[] = ["Programming Languages", "Java", "JavaScript", "TypeScript", "Python", "Swift"];
  private sports: string[] = ["Sports", "Tennis", "Football", "Basketball", "Hockey", "Track"];
  private foods: string[] = ["Foods", "CheeseBurger", "banana", "Pizza", "Cereal", "STEAK"]
  private misc: string[] =["Misc", "goodbye", "Mystery", "Jazz", "blizzard", "quiz"]
  private challenge: string[];
  private categoryArray: string[][];

  private alphabet: string[] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
    "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
  private currentGuessElement: HTMLElement;
  private gameOverWords: HTMLElement;
  private gameWinWords: HTMLElement;
  private lives: number;
  private win: boolean;
  private clue: string;

  constructor() {
    this.fetchWord();
    this.displayWord = "Press New Game";
    this.lives = 0;
    this.win = false;
  }

  newGameClick() {//chooses a random word, makes a displayWord with dashes, resets letters to visable 
    this.challenge = ["Challenge", this.randomWord, this.randomWord2, this.randomWord, this.randomWord2, this.randomWord];
    this.categoryArray = [this.cars, this.programming, this.sports, this.foods, this.challenge, this.misc];

    let eraserSound = document.getElementById('eraser') as HTMLAudioElement;
    eraserSound.currentTime = 0;
    eraserSound.play();

    this.gameOverWords = document.getElementById('gameOverWords') as HTMLElement;
    this.gameOverWords.style.visibility = "hidden";

    this.gameWinWords = document.getElementById('gameWinWords') as HTMLElement;
    this.gameWinWords.style.visibility = "hidden";
    this.lives = 6;
    this.win = false;

    this.chosenCategory = this.categoryArray[Math.floor(Math.random() * this.categoryArray.length)];//selects a category array
    this.category = this.chosenCategory[0].toString();

    this.chosenWord = this.chosenCategory[Math.floor(Math.random() * (this.chosenCategory.length - 1)) + 1].toLowerCase();//selects a word from chosen array
    this.displayWord = "";
    this.clue = this.chosenWord.charAt(Math.random() * this.chosenWord.length).toUpperCase();

    for (let i = 0; i < this.chosenWord.length; i++) {
      //add in spaces if present
      if(this.chosenWord.charAt(i) !== " "){
        this.displayWord = this.displayWord + "-";
      }
      else{
        this.displayWord = this.displayWord + " ";
      }
    }

    this.currentGuessElement = document.getElementById("wordToGuess");
    this.currentGuessElement.innerHTML = this.displayWord;
    this.displayLetters();
    let labelElement = document.getElementById('livesLabel');
    labelElement.style.visibility = "visible";
    this.drawMan();
  }

  endGameClick() {
    try {
      this.currentGuessElement.innerHTML = "Press New Game";
      this.lives = 0;
      this.win = false;
      this.gameOverWords.style.visibility = "visible";
      this.gameWinWords.style.visibility = "hidden";
      this.displayLetters();
      this.drawMan();
    } catch (e) {
      console.error("Start game first" +e);
    }
  }

  @HostListener('document:click', ['$event'])
  letterClick(e: MouseEvent) {
    if (this.win === false) {

      if (this.lives !== 0) {
        let myclass: string = (e.target as HTMLElement).className;

        if (myclass === "letters") {
          let element = document.getElementById((e.target as HTMLElement).id);
          element.style.visibility = "hidden";
          this.checkWithWord(this.chosenWord, this.displayWord, (e.target as HTMLElement).id);
        }
      }
    }
    try {
      if (this.lives === 0) {
        this.gameOverWords.style.visibility = "visible";
        this.currentGuessElement.innerHTML = this.chosenWord;
        let x = document.getElementById('hung') as HTMLAudioElement;
        x.play();
      }
    } catch (e) {
      console.error("Start game first" +e);
    }
  }

  checkWithWord(chosenWord: string, displayWord: string, letterid: string) {
    chosenWord = this.chosenWord;
    displayWord = this.displayWord;
    let isLetter = false;

    for (let i = 0; i <= chosenWord.length; i++) {
      if (chosenWord.charAt(i) === letterid) {
        //change char at displayword to char at chosen word...
        displayWord = this.setCharAt(displayWord, i, letterid);
        this.displayWord = displayWord;
        //console.log("the letter " + letterid + " is in there");
        isLetter = true;
      }
    }
    this.currentGuessElement.innerHTML = displayWord;
    if (!isLetter) {
      this.lives -= 1;
      let x = document.getElementById('chalk') as HTMLAudioElement;
      x.play();
    }
    this.checkForWin();
    this.drawMan();
  }

  setCharAt(str: string, index: number, chr: string) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
  }

  displayLetters() {
    for (let index = 0; index < 26; index++) {
      let element = document.getElementById(this.alphabet[index]) as HTMLElement;
      element.style.visibility = "visible";
    }
  }

  checkForWin() {
    let chosenWord = this.chosenWord;
    let displayWord = this.displayWord;
    let lettersLeft = chosenWord.length;

    for (let i = 0; i < chosenWord.length; i++) {
      if (chosenWord.charAt(i) === displayWord.charAt(i)) {
        lettersLeft -= 1;
      }
    }
    if (lettersLeft === 0) {
      this.win = true;
    }
    if (this.win) {
      this.gameWinWords.style.visibility = "visible";
      let element = document.getElementById('livesLabel') as HTMLElement;
      element.style.visibility = "hidden";
    }
  }

  drawMan() {
    this.lives = this.lives;
    let picture = document.getElementById("pic") as HTMLImageElement;
    picture.src = `./assets/Hangman${this.lives}.gif`;
  }

  techClickOn() {
    let element = document.getElementById("techUsed");
    element.style.visibility = "visible";
  }

  async fetchWord() {
    await fetch('http://api.wordnik.com/v4/words.json/randomWord?api_key=bj8rr9l42eb8frq3lvmiwrrryn5k9m8j4igg7apdts8l69vpl')
      .then(response => response.json())
      .then(data => {
        this.randomWord = data.word as string;
        console.log(this.randomWord); 
      })
      await fetch('http://api.wordnik.com/v4/words.json/randomWord?api_key=bj8rr9l42eb8frq3lvmiwrrryn5k9m8j4igg7apdts8l69vpl')
      .then(response => response.json())
      .then(data =>{
        this.randomWord2 = data.word as string;
        console.log(this.randomWord2);
      })

  }

}