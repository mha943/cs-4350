"use strict";
exports.__esModule = true;
var Main = /** @class */ (function () {
    function Main() {
        var _this = this;
        this.beginDraw = false;
        this.previousX = 0;
        this.currentX = 0;
        this.previousY = 0;
        this.currentY = 0;
        this.dotFlag = false;
        this.fillColor = "black";
        this.stroke = 2;
        this.canvas = document.getElementById("canvas");
        this.ctx = this.canvas.getContext("2d");
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.canvas.addEventListener("mousemove", function (e) {
            _this.findxy("move", e);
        });
        this.canvas.addEventListener("mousedown", function (e) {
            _this.findxy("down", e);
        });
        this.canvas.addEventListener("mouseup", function (e) {
            _this.findxy("up", e);
        });
        this.canvas.addEventListener("mouseout", function (e) {
            _this.findxy("out", e);
        });
        var colorDivs = document.getElementsByClassName("color-item");
        for (var _i = 0, _a = colorDivs; _i < _a.length; _i++) {
            var div = _a[_i];
            div.addEventListener("click", function (e) {
                var targetElement = e.target;
                _this.color(targetElement.id);
            });
        }
        document.getElementById("save").addEventListener("click", function () {
            _this.save();
        });
        document.getElementById("clear").addEventListener("click", function () {
            _this.clear();
        });
    }
    Main.prototype.color = function (color) {
        this.fillColor = color;
        if (this.fillColor === "white") {
            this.stroke = 20;
        }
        else {
            this.stroke = 2;
        }
    };
    Main.prototype.draw = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(this.previousX, this.previousY);
        this.ctx.lineTo(this.currentX, this.currentY);
        this.ctx.strokeStyle = this.fillColor;
        this.ctx.lineWidth = this.stroke;
        this.ctx.stroke();
        this.ctx.closePath();
    };
    Main.prototype.clear = function () {
        var doesWantToClear = confirm("Are you sure you want to clear?");
        if (doesWantToClear) {
            this.ctx.clearRect(0, 0, this.width, this.height);
            document.getElementById("canvasimg").style.display = "none";
        }
    };
    Main.prototype.save = function () {
        var img = document.getElementById("canvasimg");
        img.style.border = "2px solid";
        var dataUrl = this.canvas.toDataURL();
        img.src = dataUrl;
        img.style.display = "inline";
    };
    Main.prototype.findxy = function (eventType, e) {
        // console.log(eventType);
        if (eventType === "down") {
            this.previousX = this.currentX;
            this.previousY = this.currentY;
            this.currentX = e.clientX - this.canvas.offsetLeft;
            this.currentY = e.clientY - this.canvas.offsetTop; //so we dont draw in middle of screen
            this.beginDraw = true;
            this.dotFlag = true;
            if (this.dotFlag) {
                this.ctx.beginPath();
                this.ctx.fillStyle = this.fillColor;
                this.ctx.fillRect(this.currentX, this.currentY, this.stroke, this.stroke);
            }
        }
        if (eventType === "up" || eventType === "out") {
            this.beginDraw = false;
        }
        if (eventType === "move") {
            if (this.beginDraw) {
                this.previousX = this.currentX;
                this.previousY = this.currentY;
                this.currentX = e.clientX - this.canvas.offsetLeft;
                this.currentY = e.clientY - this.canvas.offsetTop;
                this.draw();
            }
        }
    };
    return Main;
}());
exports.Main = Main;
new Main();
